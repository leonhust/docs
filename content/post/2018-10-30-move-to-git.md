---
title: 把工程从svn迁移到git
date: 2018-10-31
tags: ["git", "svn"]
---

**项目迁移流程**

1. 提交所有未commit的代码到svn
2. 清理掉不需要的branch
3. 文件编码统一转换到utf8（针对所有需要的branch）
    ```bash
    for file in `find . -type f`;
    do
            if file -i ${file} | grep -q text;
            then
                    if ! file -i ${file} | grep -q charset=utf-8;
                    then
                            echo not utf8 `file -i ${file}`
                            tmpfile=${file}.tmp
                            iconv -f GB18030 -t UTF-8 ${file} -o ${tmpfile}
                            if [ $? -eq 0 ]
                            then
                                    mv ${tmpfile} ${file}
                            fi
                    fi
            fi

    done
    ```
    少量文件会出错，用`svn st|grep ^?`查看一下，手动处理。
4. 用git clone svn仓库
   1. 获取svn作者名字列表

        ```
        svn log --xml | grep author | sort -u | perl -pe 's/.*>(.*?)<.*/$1 = /' > user.txt
        ```
        导出并补充完整，如：
        ```
        gaojie = Gao Jie <jie.gao@ivocap.com>
        leidi = Lei Di <di.lei@ivocap.com>
        maxindong = Ma Xindong <xindong.ma@ivocap.com>
        songluming = Song Luming <luming.song@ivocap.com>
        wangzhengyong = Wang Zhengyong <zhengyong.wang@ivocap.com>
        xiongyongqiang = Xiong Yongqiang <yongqiang.xiong@ivocap.com>
        yangyi = Yang Yi <yi.yang@ivocap.com>
        zhangliang = Zhang Liang <hansen.zhang@ivocap.com>
        ```
    2. ubuntu需要安装`git-svn`包
        ```
        sudo apt-get install git-svn
        ```
    3. 克隆svn仓库，并建立本地分支
        ```
        git svn clone http://192.168.0.151/svn/ivocap/MdServer/code --authors-file=../user.txt --no-metadata -s MdServer.git
        git checkout -b mdserver_v2 remotes/origin/mdserver_v2
        ```
        导出的仓库具有完整的svn提交历史。有2点需要注意：
        - svn路径要填对，填trunk,tags,branches的父目录。
        - 没有标准目录的项目，导入时不能用`-s`参数，需要指定`-T`，`-b`和`-t`来分别指定trunk，branches和tags，譬如：
  
            ```
            git svn clone http://192.168.0.151/svn/yy --authors-file=../user.txt --no-metadata -T CffexTradeMonitor TradeMonitor.git
            ``` 
        - 根据远程分支checkout出本地分支，需要把所有的远程分支checkout出来，否则push到远程仓库后没有对应的分支。
        - 如果有些项目要把其他分支设置为master分支，这是一个好时机：
   
            ```
            git branch -m master legacy
            git branch -m for_orderid_refactor master
            ``` 

5. 把git仓库推到远程公共仓库
    1. 到gitlab上建一个新的空项目，假设地址为

        > http://192.168.0.160/ivocap/mdserver.git
    2. 添加远程仓库，并把clone出来的项目push到远程仓库
        ```
        cd MdServer.git
        git remote add origin http://192.168.0.160/ivocap/mdserver.git
        git push -u origin --all
        ```

后续把`.gitignore`，`README.md`等设好。