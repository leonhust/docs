---
title: HUGO
date: 2018-10-31
tags: ["hugo"]
---

本地安装hugo。

# 安装

从[这里](https://github.com/gohugoio/hugo/releases)下载windows二进制，解压到`C:\Program Files\hugo`，并设置好环境变量。

# 启动
进入站点根目录，启动server
```bash
cd  /e/project/git/docs
hugo server
```


用浏览器打开[http://localhost:1313/docs/](http://localhost:1313/docs/)预览。改动md文件保存的时候，浏览器内容会自动更新，完美。

测试完推送到gitlab，自动触发编译部署。

# 常用语法
- 不想显示到网页的注释看起来只有这种方法
```
<!-- this is a comment -->
```


# 问题
不支持markdown `mermaid`流程图。在其他地方渲染后贴图，好在有本地预览，也比较方便。