---
title: git开发流程
date: 2018-10-31
tags: ["git", "workflow"]
---



用docker跑了一个gitlab ee版本在`192.168.0.160`上。相对于github的优势：
* gitlab权限控制更好。
* 本地速度更快。
* 更可控。
* 免费。
* wiki可以截图，上传附件，支持mermaid格式流程图，可以作为理想的需求文档宿主。

# 熟悉Git
[Pro Git中文版](https://git-scm.com/book/zh/v2)

# 登陆Gitlab
* 浏览器打开`192.168.0.160`
* 注册用户
* 把自己的ssh pub key放入User=>Settings=>SSH Keys（如果仅通过`http://`方式来克隆仓库则不需要）

# 开发流程
### 需要review流程的项目
鉴于团队较小，可以使用[功能分支工作流](https://github.com/xirong/my-git/blob/master/git-workflow-tutorial.md#22-%E5%8A%9F%E8%83%BD%E5%88%86%E6%94%AF%E5%B7%A5%E4%BD%9C%E6%B5%81)这一模型，在提供review功能的前提下，流程比较简单。

若以后有需求，可以无缝切到[Gitflow工作流](
https://github.com/xirong/my-git/blob/master/git-workflow-tutorial.md#23-gitflow%E5%B7%A5%E4%BD%9C%E6%B5%81)。

`功能分支工作流`主要思想是所有开发者使用**同一个远程公共仓库**，`master分支`**不做**功能开发。开发全部在其他`功能分支`上，开发完后把`功能分支`push到远程公共仓库，发起pull request进行code review，review通过，`maintainer`把它merge到`master分支`。

<font color=#ff0000 size=14 face="黑体">千万不要直接push修改到master分支。</font>

gitlab默认给`master分支`设定了保护属性（可以在`项目=>Settings=>Repository=>Protected Branches`里修改默认设置），只有`Maintainer`有修改公共仓库`master分支`的权利。在gitlab里给项目合作者分配`Developer`角色，可以保证开发者只能push修改到其他分支，经过review后由`Maintainer`合并入`master分支`。

### 不需要review流程的项目
个人项目或文档项目不需要pre-commit的review的，可以使用最简单的[集中式工作流](https://github.com/xirong/my-git/blob/master/git-workflow-tutorial.md#21-%E9%9B%86%E4%B8%AD%E5%BC%8F%E5%B7%A5%E4%BD%9C%E6%B5%81)，这也是我们熟悉的svn工作流。gitlab里可以把所有项目合作者设置为`Maintainer`，或者不要把`master分支`设置为Protected，让所有协作者可以直接把修改push到master分支。
当项目又需要pre-commit review时，可以无缝切换到功能分支工作流。

# `功能分支工作流`的典型开发case
#### 基于master分支开发新功能：
1. 克隆仓库(根据提示输入注册的用户名和密码，如果是别人的私有仓库，需要别人在项目里邀请)

    ```
    git clone http://192.168.0.160/leonhust/testproject.git
    ```
2. 同步最新代码（如果是刚刚clone出来的仓库，可以跳过这一步）
```
git fetch origin
```
3. 基于origin/master创建新分支并切换过去（假设分支名为new）
``` 
git checkout -b new origin/master
```
4. 完成功能
5. 提交修改到本地分支
```
git add xxx
git commit -m "xxx"
```
6. 把新分支push到远程公共仓库
```
git push -u origin new
```
7. 发起pull request

    > 登陆gitlab，到该项目new分支下，发起到master分支的merge request。

8. 删除功能分支
```
git branch -d new
```

如果review通过，代码merge进入master，功能分支需要删掉（最好由项目维护者做）。如果要加其他功能，另创一个分支开发。

如果还在该分支开发功能，master分支有其他提交并入，难以rebase维护分支历史。

#### 修改别人的远程分支
1. 同步远程代码（假设已经clone过了，如果没有clone，参考上面的步骤clone仓库）

    ```
    git fetch origin
    ```
2. 在远程分支的基础上，创建本地分支，并切换到本地分支
```
git checkout -b new origin/new
```
3. 完成功能
4. 提交修改到本地分支
```
git add xxx
git commit -m "xxx"
```
5. push修改到远程分支
```
git push
```
6. 发起pull request
 

#### 和别人的提交有冲突
如果公共仓库`master分支`已经merge了别人的代码，导致和自己的提交有冲突，pull request被打回，需要重新rebase到origin/master，解决冲突并重新提交。

1. 同步远程代码
    ```
    git fetch origin
    ```
2. 切换到本地的功能分支
    ```
    git checkout new
    git pull
    ```
3. `merge`远程master分支
    ```
    git merge origin/master
    ```
4. 手动解决冲突
5. 提交修改
    ```
    git add xxx
    git commit -m "xxx"
    ```
6. push修改到远程分支
    ```
    git push
    ```
7. 发起pull request

#### 项目维护者处理merge request
**通过网页**

*TODO*

**通过命令**

1. 同步远程代码
```
git fetch origin
```
2. 切换到功能分支检查一下代码改动（可选）
```
git checkout -b new origin/new
```

3. 切换到master分支，并保证和远程分支一致
```
git checkout master
git pull
```
4. merge功能分支
```
git merge --no-ff origin/new
```
5. 如果没有冲突，或者解决了冲突并commit到本地master分支，直接push修改到远程master分支，并删除功能分支
```
git push
git push --delete origin new
```

**关于merge冲突**

如果上述第4步merge有冲突，且冲突比较简单，项目维护者可以自己解决冲突。若比较复杂，可以关闭pull request，让开发者解决冲突后再次提交pull request。

# `集中式工作流`的典型开发case
`集中式工作流`不区分协作者角色，所有协作者都基于`master分支`开发，并可以直接push修改到公共仓库`master分支`，所以大家都遵循以下流程。整个过程和svn非常类似，仅增加了commit代码到本地仓库的步骤。



1. 克隆仓库（若已经克隆，跳过）

    ```
    git clone http://192.168.0.160/leonhust/testproject.git
    ```
2. 同步最新代码（如果是刚刚clone出来的仓库，可以跳过这一步），类似执行`svn update`

    ```
    git pull --rebase
    ```
3. 开发功能并提交到master分支

    ```
    git commit -am "msg"
    ```
4. 再次同步最新代码（可能上面开发过程有人已经往远程仓库提交了代码），类似执行`svn update`

    ```
    git pull --rebase
    ```
5. 若有冲突解决冲突，解决完后继续rebase。
    ```
    git add xxx
    git rebase --continue
    ```
6. push修改到远程master分支
    ```
    git push
    ```

流程图如下：

![集中式工作流流程图](../images/flowchart_centralized_workflow.png)

以上流程图代码：
```mermaid
graph TD
S(Start)
E(End)
IsRepoExist{本地仓库存在?}
CloneRepo[git clone url]
Update[git pull --rebase]
Implement[实现功能]
Commit[git commit -am msg]
UpdateAgain[git pull --rebase]
IsConflict{是否有冲突?}
FixConflict[ 解决冲突并执行git add xxx ]
RebaseContinue[git rebase --continue]
Push[git push]

S -->> IsRepoExist;
IsRepoExist-->|否|CloneRepo
IsRepoExist-->|是|Update
Update-->Implement
CloneRepo-->Implement
Implement-->Commit
Commit-->UpdateAgain
UpdateAgain-->IsConflict
IsConflict-->|是|FixConflict
IsConflict-->|否|Push
FixConflict-->RebaseContinue
RebaseContinue-->Push
Push-->E
```


# git图形客户端
命令行是能发挥git所有威力的地方，但门槛略高。所有图形界面都只是git功能的一个子集，是针对某些工作流而开发的。需要找一款能适应我们流程的工具。

*TODO*


# 从svn迁移到git
### 为什么要迁移到git
网上有很多关于svn和git的比较，譬如[这篇](https://github.com/xirong/my-git/blob/master/why-git.md)，但对于我们来说其主要好处是为顺畅的流程提供了可能。详见：
[#task-5bd660f49ecfa83b9ed648e0|关于改进代码质量的思考]

有人可能会顾虑git比svn复杂很多。git的复杂性源于它提供了比svn更多的灵活性和可能性。如果只是完成svn的功能，git可以使用集中式工作流，也可以很简单。而如果用svn实现git的功能，有些根本做不到，有些流程会更复杂。

### 如何迁移
为了能平滑过渡到git，可以分以下四个阶段推进：
1. 在git测试项目上，熟悉集中式工作流
2. 选一个项目迁移到git上，仍使用集中式工作流
3. 所有项目迁移到git上，仍使用集中式工作流
4. 选出需要协作执行review的项目，切到功能分支工作流。

**项目迁移流程**
1. 提交所有未commit的代码到svn
2. 清理掉不需要的branch
3. 文件编码统一转换到utf8（针对所有需要的branch）
4. 用git clone svn仓库
5. 把git仓库推到远程公共仓库

# 最佳实践
这些是我自己实践出来的一些体会，大家可以一起改进和完善。不是强制要求，但如果所有人都能遵守一些共同的原则，能节省所有人的时间并且让事情变得轻松。
* 提交原则：一个功能一个提交，不要一个提交里包含很多功能。方便其他review的人理解你的修改，要revert某些修改也很容易。
* 不要push -f
* 不要rebase掉已经提交到远程仓库的提交
* 功能分支merge了删除掉
* merge/rebase时确保工作目录是整洁的，没有未提交的更新，如果merge有冲突解决起来容易些。

# FAQ

**如何查看本地和远程分支？**
```
git branch -a
```
推荐用如下参数，还能看到简短的最后提交
```
git branch -av
```
**如何删除本地和远程分支？**
```
git branch -d <branch_name>
git push --delete <remote_name> <branch_name>

e.g.: git branch -d new
      git push --delete origin new
```
**如何删除已merge的本地分支？**
```
git branch --merged
git branch -d <branch_name>
```
**如果其他人删除了公共仓库的远程分支，我本地还能看到，怎么办？**
```
git fetch --prune
```
**如何寻找2个分支最近的共同祖先？**
```
git merge-base <branch_a> <branch_b>
```